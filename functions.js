var url_facets = "http://phpdev2.dei.isep.ipp.pt/~nsilva/imorest/facetas.php";
var url_facets_add_per_value = "http://phpdev2.dei.isep.ipp.pt/~nsilva/imorest/imoveis.php?";
var url_facets_values = "http://phpdev2.dei.isep.ipp.pt/~nsilva/imorest/valoresFaceta.php?faceta=";
var url_facets_metadata = "http://phpdev2.dei.isep.ipp.pt/~nsilva/imorest/tipoFaceta.php?faceta=";
var url_facets_cont_max = "http://phpdev2.dei.isep.ipp.pt/~nsilva/imorest/maxFaceta.php?facetaCont=";
var url_facets_cont_min = "http://phpdev2.dei.isep.ipp.pt/~nsilva/imorest/minFaceta.php?facetaCont=";

var facetas = [];
var contentor_widget;

/* Função encarergue de iniciar a widget
   param: tag_id -> id da tag a substituir na pagina hospedeira */
function init(tag_id)
{
	makeXmlHttpGetCall(url_facets, null, true, true, getFacetasFromXML,[tag_id]);
}

/* Cria objecto XmlHttpRequest consoante o browser */
function createXmlHttpRequestObject(){
	var xmlhttp;
	
	if (window.XMLHttpRequest) {
		xmlhttp = new XMLHttpRequest();
	} else {
		xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	}
	return xmlhttp;
}

/* Função de pedidos ajax */
function makeXmlHttpGetCall(url, params, async, xml, callback, args)
{
	var xmlHttpObj = createXmlHttpRequestObject();
	if (xmlHttpObj){
		xmlHttpObj.open("Get",url, async);
		xmlHttpObj.onreadystatechange = function() {
			if (xmlHttpObj.readyState == 4 && xmlHttpObj.status == 200) {
				var response;
				if (xml == true){
					response = xmlHttpObj.responseXML;
				}else{
					response = xmlHttpObj.responseText;
				}	
				callback(response,args);
			}
		};
		xmlHttpObj.send(params);	
	}
}

/* Função responsável por enviar pedido ajax para carregar as facetas */
function loadFacetas(){
	makeXmlHttpGetCall(url_facets, null, false, true, getFacetasFromXML,[]);
}


/* Recebe o response.XML e introduz os nomes das facetas na variavel global 'facetas' */
function getFacetasFromXML(responseXML,tag_id){
	var arrayFacetas = [];
	var xmlDoc = responseXML;
	var nodeList = xmlDoc.getElementsByTagName('faceta');
	for(var i = 0, len = nodeList.length; i < len; i++){
		arrayFacetas[i] = nodeList[i].childNodes[0].nodeValue;
	}
	facetas = arrayFacetas;
	assembleForm(tag_id);
}

/* Função encarregue de enviar pedido ajax para obter os valores das facetas */
function getValoresFacetasAjax(){
	
	for(var i = 0, len = facetas.length; i < len; i++){
		var values = [];
		var url = url_facets_values + facetas[i];
		makeXmlHttpGetCall(url, null, true, false, getValoresFacetasJSON, [facetas[i]]);	
	}	
}

/* Função que faz o parse da JSON recebido pelo pedido e cria as dropdown list com as opções */
function getValoresFacetasJSON(responseText,facetaName)
{
	var values = [];
	var response = JSON.parse(responseText);
	for(var i = 0, len = response.length; i < len; i++){
		values[i] = response[i];
	}
	createSelectWithOptions(facetaName,values,"Seleccione a opção");
}
	
/* Funcção de callback para carregar os metadados e encaminhar para criar os inputs*/
function getMetaDadosJSON(responseText, facetaName){
	var response = JSON.parse(responseText);
	var max,min,symbol;
	if(isDiscreto(response)){
		var url = url_facets_values + facetaName;
		makeXmlHttpGetCall(url, null, true, false, getValoresFacetasJSON, [facetaName]);
	}else{
		var url_max = url_facets_cont_max + facetaName;
		var url_min = url_facets_cont_min + facetaName;
		symbol = getSemantic(response);
		createInputMaxMin(facetaName,0,0,symbol);
		makeXmlHttpGetCall(url_max, null, true,false, updateMaxValue ,[facetaName]);
		makeXmlHttpGetCall(url_min, null, true,false, updateMinValue ,[facetaName]);
	}
	
}

/* Atualiza o valor máximo da faceta passada por parametro*/ 
function updateMaxValue(response, facetaName)
{	
	var response = JSON.parse(response);
	var max = response.max;
	var inputMax = document.getElementById(facetaName + "-max");
	inputMax.setAttribute("value", max);
	inputMax.setAttribute("max", max);
}
/* Atualiza o valor min da faceta passada por parametro*/ 
function updateMinValue(response, facetaName)
{
	var response = JSON.parse(response);
	var min = response.min;;
	var inputMin = document.getElementById(facetaName + "-min");
	inputMin.setAttribute("value", min);
	inputMin.setAttribute("min", min);
}

/* Pedido ajax dos metadados JSON */
function drawInputUI()
{
	for(var i = 0, len = facetas.length; i < len; i++){
		var url = url_facets_metadata + facetas[i];
		makeXmlHttpGetCall(url, null, true, false, getMetaDadosJSON, [facetas[i]]);
	}
}

/* Retorna a semantica dos dados */
function getSemantic(data)
{
	if (data.semântica == "espaço"){
		return "m^2";
	}
	if(data.semântica == "monetário"){
		return "€";
	}
	return "";
}
/* Verifica se e' discreto */
function isDiscreto(data){
	if (data.discreto == "discreto"){
		return true;
	}else{
		return false;
	}
}

/* Cria os textfields para maximo e minimo */
function createInputMaxMin(facetaName, min, max, simbol){
	//criar div
	var div = createDiv("elemento_"+facetaName+"_id","");
	
	// criar dois inputs para o maximo e o minimo
	var inputMax = document.createElement("input");
	var inputMin = document.createElement("input");
	
	var inputMaxId = facetaName + "-max";
	var inputMinId = facetaName + "-min";
	
	inputMax.setAttribute("type","number");
	inputMax.setAttribute("id", inputMaxId);
	
	inputMin.setAttribute("type","number");
	inputMin.setAttribute("id", inputMinId);
	
	var labelMax = createLabel("",inputMaxId,"Max(" + simbol + "): ");
	var labelMin = createLabel("",inputMinId,"Min(" + simbol + "): ");
	
	div.setAttribute("style", "display: none;");
	var elemento = document.getElementById("li_"+facetaName);
	div.appendChild(labelMin);
	div.appendChild(inputMin);
	div.appendChild(labelMax);
	div.appendChild(inputMax);
	elemento.appendChild(div);
}
/*	
Cria um dropdown menu passando como parametro o nome da faceta,
os seus valores e um valor por defeito

*/
function createSelectWithOptions(facetaName,values,defaultValue)
{
	var div = createDiv("div"+facetaName+"id");
	var select = document.createElement("select");
	var option;
	var selectId = "elemento_"+facetaName+"_id";
	select.setAttribute("id", selectId);
	select.setAttribute("name", facetaName + "_select");
	select.setAttribute("class", "element select medium");
	select.setAttribute("multiple",true);
	// Adicionar valor por defeito
	option = document.createElement("option");
	option.setAttribute("value", "");
	option.setAttribute("selected", "selected");
	select.appendChild(option);
	
	//Adicionar valores da faceta
	for (var i = 0, len = values.length; i < len; i++)
	{
		option = document.createElement("option");
		option.value = values[i];
		option.text = values[i];
		select.appendChild(option);
	}
	// adiciona div a' item list
	var elemento = document.getElementById("li_"+facetaName);
	select.setAttribute("style", "display: none;");
	elemento.appendChild(select);
}

/* Elimina o conteudo da tag passada por parametro. Usado para limpar a seccao onde vai ser inserida a widget */
function eraseContent(idTag)
{
	var myNode = document.getElementById(idTag);
	while (myNode.firstChild) {
		myNode.removeChild(myNode.firstChild);
	}
}

/* Chama a funcao eraseContent() e cria a base do form */
function assembleForm(tag_id)
{
	eraseContent(tag_id);
	var elemento_root = document.getElementById(tag_id);
	
	contentor_widget = createDiv("form_container", "");
	var form_title = createHeading("Pesquisa de ImÃ³veis",3);
	
	// criaÃ§ao da tabela de resultados vazia
	var result_table = document.createElement("table");
	result_table.setAttribute("id","results_table_id");
	result_table.setAttribute("class","tftable");
	result_table.setAttribute("border", "1");
	
	var form = createForm("form_widget", "","POST");
	var ul = createUnorderedList("lista_elementos");
	form.appendChild(ul);
	
	for(var i = 0, len = facetas.length - 1; i < len; i++){
		var li = createListItem("li_"+facetas[i]);
		var forWho = "elemento_"+facetas[i]+"_id";
		var label = createLabel("",forWho,facetas[i]);
		var cb = createCheckBox(i);
		label.appendChild(cb);
		li.appendChild(label);
		ul.appendChild(li);
	}
	var botao = createButton("getValuesFromForm()");
	ul.appendChild(botao);
	
	contentor_widget.appendChild(form);
	contentor_widget.appendChild(result_table);
	elemento_root.appendChild(form_title);
	elemento_root.appendChild(contentor_widget);
	drawInputUI();
}

/*cria cada checkbox*/
function createCheckBox(i){
	var cb = document.createElement("input");
	cb.setAttribute("type", "checkbox");
	cb.setAttribute("id", facetas[i]);
	cb.setAttribute("onclick","onclickChange(this)");
	cb.setAttribute("checked", true);
	return cb;
}

/*faz o evento para cada checkbox*/
function onclickChange(cb){
	if(cb.checked){
		var a ="elemento_"+cb.getAttribute('id')+"_id";
		document.getElementById(a).style.display = "none";
	}else{
		var a ="elemento_"+cb.getAttribute('id')+"_id";
		document.getElementById(a).style.display = "block";
	}
	return;
}


function createHeading(text,size)
{
	var title = document.createElement("h"+size);
	var texto = document.createTextNode(text);
	title.appendChild(texto);
	return title;
}

function createDiv(tagId, css_class)
{
	var div = document.createElement("div");
	if(tagId !== 'undefined' || tagId !== ""){
		div.setAttribute("id",tagId);
	}
	if(css_class !== 'undefined' || css_class !== ""){
		div.setAttribute("class",css_class);
	}
	return div;
}

function createForm(id,css_class,method)
{
	var form = document.createElement("form");
	form.setAttribute("class",css_class);
	form.setAttribute("id",id);
	form.setAttribute("method",method);
	return form;
}

function createLabel(css_class, forwho, text)
{
	var label = document.createElement("label");
	if(css_class !== ""){
		label.setAttribute("class",css_class);
	}
	label.setAttribute("for", forwho);
	var texto = document.createTextNode(text);
	label.appendChild(texto);
	return label;
}

function createListItem(id,css_class)
{
	var listItem = document.createElement("li");
	listItem.setAttribute("id", id);
	listItem.setAttribute("class", css_class);
	return listItem;
}

function createUnorderedList(id)
{
	var list = document.createElement("ul");
	list.setAttribute("id",id);
	return list;
}

function createButton(funcao)
{
	var botao = document.createElement("input");
	botao.setAttribute("id","button_id");
	botao.setAttribute("class","button_text");
	botao.setAttribute("type","button");
	botao.setAttribute("value","Pesquisar");
	botao.setAttribute("name", "pesquisar");
	botao.setAttribute("onclick",funcao);
	return botao;
}

/* Retira os dados dos inputs e chama a funÃƒÂ§ÃƒÂ£o searchAjaxRequest */
function getValuesFromForm(){
	var elementos = [];
	var search_string = "";
	var aux;
	var id;
	var input;
	
	for(var i = 0, len = facetas.length-1; i < len; i++){
		var a=0;
		var b=0;
		id = "elemento_" + facetas[i] + "_id";
		input = document.getElementById(id);
		if(input.nodeName === "SELECT"){
			if(!input.options[0].selected){
				for (var u = 1; u < input.options.length; u++) {
					if (input.options[u].selected) {
						a++;
					}
				}
				search_string += facetas[i] + "=[";
				for (var j = 1; j < input.options.length; j++) {
					if (input.options[j].selected) {
						if(a==1){
							aux = input.options[j].value;
							search_string += aux;
						}else if (a==2){
							if(b==0){
							aux = input.options[j].value;
							search_string += aux;
							search_string+=",";
							b++;
							}else{
								aux = input.options[j].value;
								search_string += aux;
							}
						}else if (a==3){
							if(b==0 || b==1){
							aux = input.options[j].value;
							search_string += aux;
							search_string+=",";
							b++;
							}else{
								aux = input.options[j].value;
								search_string += aux;
							}
						}
						
					}
				}
				search_string+="]&";
			}
		}		
	}
	var url = url_facets_add_per_value + search_string;
	searchAjaxRequest(search_string);
}

/* função responsavel por efetuar o pedido dos resultados da pesquisa*/
function searchAjaxRequest(search_string){
	var url = url_facets_add_per_value + search_string;
	makeXmlHttpGetCall(url, null, true, false, createResultsTable, []);
}

/* Cria a tabela de resultados */
function createResultsTable(responseText){
	var response = JSON.parse(responseText);
	var oldTable = document.getElementById('results_table_id');
	
	var newTable = oldTable.cloneNode(true);
	
	// Remove child nodes
	while(newTable.hasChildNodes()){
		newTable.removeChild(newTable.firstChild);
	}
	// Cria os headings da tabela
	for(var i = 0; i < facetas.length; i++){
		var th = document.createElement("th");
		th.appendChild(document.createTextNode(facetas[i]));
		newTable.appendChild(th);
	}
	
	// Cria as linhas da tabela
	for(var item in response){
		var tr = document.createElement('tr');
		for(var subItem in response[item]){
			var td = document.createElement('td');
			td.appendChild(document.createTextNode(response[item][subItem]));
			tr.appendChild(td);
		}
		newTable.appendChild(tr);
	}
	
	oldTable.parentNode.appendChild(newTable);
	filtraTabelaResultados(newTable);
	oldTable.parentNode.removeChild(oldTable);
}

/* Filtra os resultados da tabela */
function filtraTabelaResultados(table)
{
	var rows = table.rows;
	var rowcount = rows.length;
	var precoIndex;
	var areaIndex;
	for(var j = 0; j < facetas.length; j++){
		if(facetas[j] == "preço"){
			precoIndex = j;
		}
		if(facetas[j] == "área"){
			areaIndex = j;
		}
	}
	
	for(var i = 0; i < rowcount; i++){
		var flagPreco = valueBetweenMinMax(rows[i].cells[precoIndex].childNodes[0].data, facetas[precoIndex]);
		var flagArea = valueBetweenMinMax(rows[i].cells[areaIndex].childNodes[0].data, facetas[areaIndex]);
		if(flagPreco == false || flagArea == false){
			table.deleteRow(i);
			rowcount--;
			i--;
		}
	}
	return table;
}
/* Retorna true se o valor estiver dentro dos limites dos respetivos maximos e minimos da faceta */ 
function valueBetweenMinMax(value,faceta)
{
	var valueInt = parseInt(value);
	var min = parseInt(document.getElementById(faceta + "-min").value);
	var max = parseInt(document.getElementById(faceta + "-max").value);
	
	if(value === 'undefined' || value === ""){
			return true;
	}
	if(valueInt >= min && valueInt <= max){
			return true;
	}else{
		return false;
	}
}